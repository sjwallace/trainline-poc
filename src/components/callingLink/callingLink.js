import React from 'react';
import { Link } from 'react-router';

const CallingLink = ({ path, children }) => (
	<Link to={path.replace(/.*\.com\/(.*)/, '/$1')}>
		{children}
	</Link>
)

export default CallingLink;
