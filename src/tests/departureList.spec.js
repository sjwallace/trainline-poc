import React from 'react'
import DepartureList from '../components/departureList/departureList'
import CallingLink from '../components/callingLink/callingLink'
import { shallow } from 'enzyme'

describe('<DepartureList />',() => {

	let component
	let props = {
			departures: [
				{
					realTimePlatform: '1',
					serviceOperator: 'SW',
					destination: 'foo',
					scheduledArrival: '08:31',
					onTime: true,
					callingPatternUrl: 'https://basepath/callingPattern/foo/bar'
				}
			],
			filterBy: () => { return true }
		}

	beforeEach(() => {
		component = shallow(<DepartureList departures={props.departures}/>)
	})

	afterEach(() => {
		component.unmount()
	})

	it('should render a single list item when one is provided', () => {
		expect(component.find('li').length).toEqual(1)
	})

	it('should render one <CallingLink /> component', () => {
		expect(component.find(CallingLink).length).toEqual(1)
	});

	it('should filter out departures based on the filterBy function prop', () => {
		let props = {departures: [
			{
				realTimePlatform: '1',
				serviceOperator: 'SW',
				destination: 'foo',
				scheduledArrival: '08:31',
				onTime: true,
				callingPatternUrl: 'https://basepath/callingPattern/foo/bar'
			},
			{
				realTimePlatform: '2',
				serviceOperator: 'SW',
				destination: 'foo',
				scheduledArrival: '08:31',
				onTime: true,
				callingPatternUrl: 'https://basepath/callingPattern/foo/bar'
			}
		],
		filterBy: (d) => { return d.realTimePlatform === '2' }
		}

		component = shallow(<DepartureList {...props}/>)

		expect(component.find('li').length).toEqual(1)
		expect(component.find('.platform-num').text()).toEqual('Plat. 2')
	})

	it('should not render any html if there are no departures', () => {
		component = shallow(<DepartureList departures={[]}/>)
		expect(component.find('ul').length).toEqual(0)
	})

})
