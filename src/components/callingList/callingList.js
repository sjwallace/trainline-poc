import React, { Component } from 'react'
import trainCodes from '../../json/trainCodes'
import { isOnTime, isEarlierThanNow } from '../../utils/timing'
import { Journey } from '../../components'
require("!style!css!sass!./_calling.scss")
require("!style!css!semantic-ui-grid/grid.css")

const propTypes = {
	callingPattern: React.PropTypes.object
}

const defaultProps = {
	callingPattern: {}
}

class CallingList extends Component {

	constructor(props) {
		super(props)
		// fetch calling information
		this.props.fetchCallingPattern(this.props.location.pathname)
		this.pollingInterval = null
	}

	componentWillReceiveProps(nextProps) {
		if(nextProps.callingPattern.stops) {
			window.clearInterval(this.pollingInterval)
			this.pollingInterval = window.setInterval(() => {
				this.props.fetchCallingPattern(this.props.location.pathname)
			}, 20000)
		}
	}

	componentWillUnmount() {
		window.clearInterval(this.pollingInterval)
	}

	render() {

		let { origin, destination, stops = [] } = {...this.props.callingPattern}

		return (
			<div className="column wide callingList">
				<Journey {...this.props.callingPattern} />
				<ul className="ui centered grid">
					{
						(stops || []).map((stop, index) => {
							let { activeStation, scheduledArrival, destination, onTime, delayedBy } = stop
							return (
								<li key={index}>
									<div className={`scheduledTime${activeStation ? ' active' : ''}`}>
										{scheduledArrival}
									</div>
									<div className="callingLocation">
										<div className="arrivalTime">
											<span>
												{destination}
											</span>
										</div>
										<div className="onSchedule">
											<span className="onSchedule">
												{onTime ? 'onTime' : delayedBy ? `delayed by: ${delayedBy} min${delayedBy > 1 ? 's' : ''}` : ''}
											</span>
										</div>
									</div>
								</li>
							)
						})
					}
				</ul>
			</div>
		)
	}
}

CallingList.propTypes = propTypes
CallingList.defaultProps = defaultProps

export default CallingList
