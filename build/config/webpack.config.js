var path = require('path');
var watchify = require('watchify');

module.exports = {
    // configuration
    entry: path.join(__dirname, 'src'),
    output: {
        path: path.join(__dirname, 'dist'),
        filename: "app.bundle.js"
    },
	module: {
		loaders: [{
			test: /\.jsx?$/,
			exclude: /(node_modules|build)/,
			loader: 'babel',
			query: {
				presets: ['react', 'es2015']
			}
		}]
	},
	cache: {},
	packageCache: {},
	plugin: [
		watchify
	],
	debug: 'verbose'
};
