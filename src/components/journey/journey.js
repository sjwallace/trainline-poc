import React, { PropTypes } from 'react'

const propTypes = {
	origin: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
	destination: PropTypes.oneOfType([PropTypes.string, PropTypes.func]),
	children: PropTypes.oneOfType([PropTypes.element, PropTypes.func])
}

const defaultProps = {
	origin: '',
	destination: ''
}

const journey = ({ origin, destination, children }) => {
	return origin ? (
		<div className="journey">
			<h1>{origin}</h1>
			<span>to</span>
			<h1>{destination}</h1>
			{ children }
		</div>
	) : null
}

journey.propTypes = propTypes
journey.defaultProps = defaultProps

export default journey
