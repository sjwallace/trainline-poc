import React, { Component } from 'react'
import DepartureList from './departureList/departureList'
require("!style!css!semantic-ui-grid/grid.css")

const propTypes = {
	fetchDepartures: React.PropTypes.func,
	departures: React.PropTypes.array
}

const defaultProps = {
	fetchDepartures: () => {},
	departures: []
}

class Departures extends Component {

	constructor(props) {
		super(props)
	}

	componentDidMount() {
		const { fetchDepartures } = this.props
		fetchDepartures('departures/wat')
	}

	render() {
		return (
			<div className="ui top attached segment">
				<DepartureList {...this.props} />
			</div>
		)
	}
}

Departures.propTypes = propTypes
Departures.defaultProps = defaultProps

export default Departures
