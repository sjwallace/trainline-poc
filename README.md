# Trainline Demo

Example 

### Installation

To install the stable version:

```
npm install
```

To run the application

```
npm start
```

To run the unit tests

```
npm test
```

That’s it!


### License

MIT
