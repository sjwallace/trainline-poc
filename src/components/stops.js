import React, { Component } from 'react'
import CallingList from './callingList/callingList'
require("!style!css!semantic-ui-grid/grid.css")

class Stops extends Component {

	constructor(props) {
		super(props)
	}

	render() {
		return (
			<div className="ui grid padded">
				<CallingList {...this.props} />
			</div>
		)
	}
}

export default Stops
