import { RECEIVED_CALLING_PATTERN } from '../constants/constants'
import trainCodes from '../json/trainCodes'
import { isOnTime, delayedBy } from '../utils/timing'

function getRealTimeServiceInfo(stop = {}) {
	let { departure = {} } = {...stop}
	return departure.realTime && departure.realTime.realTimeServiceInfo || {}
}

export function normalize(stop, index, stops) {

	let { scheduled = {} } = {...stop.arrival}
	let realTimeServiceInfo = getRealTimeServiceInfo(stop)
	let scheduledArrival = stop.arrival.notApplicable ? realTimeServiceInfo.realTime : scheduled.scheduledTime

	return {
		destination: trainCodes[stop.location.crs],
		onTime: isOnTime(scheduled.scheduledTime, realTimeServiceInfo.realTime),
		delayedBy: delayedBy(scheduled.scheduledTime, realTimeServiceInfo.realTime),
		scheduledArrival: scheduledArrival && new Date(scheduledArrival).toLocaleTimeString('en-GB').substr(0,5),
		activeStation: stop.arrival.notApplicable ? true : realTimeServiceInfo.hasDeparted && !getRealTimeServiceInfo(stops[index + 1]).hasDeparted
	}
}

export default function callingPattern(state = {}, action) {
	switch (action.type) {
		case RECEIVED_CALLING_PATTERN:
			let { service } = {...action.response}
			return {
				origin: trainCodes[service.serviceOrigins[0]],
				destination: trainCodes[service.serviceDestinations[0]],
				stops: (service.stops || []).map(normalize)
			}
		default:
			return state
	}
}
