export const isOnTime = (t1, t2) => {
	return t1 && t2 && new Date(t1).getTime() === new Date(t2).getTime()
}

export const delayedBy = (t1, t2) => {

	if (t1 && t2) {
		let scheduledArrival = new Date(t1).getTime()
		let actualArrival = new Date(t2).getTime()

		if (actualArrival > scheduledArrival) {
			return (actualArrival - scheduledArrival) / 1000 / 60
		}
	}
	
	return false
}

export const isEarlierThanNow = (t1, t2) => {
	return t1 && t2 && new Date(t1).getTime() < new Date(t2).getTime()
}
