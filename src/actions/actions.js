import * as CONSTANTS from '../constants/constants'
import 'whatwg-fetch'

export function getDepartures(endpoint) {
	return onServiceCall(endpoint, CONSTANTS.RECEIVED_DEPARTURES)
}

export function getCallingPattern(endpoint) {
	// route user to the calling list route
	return onServiceCall(endpoint, CONSTANTS.RECEIVED_CALLING_PATTERN)
}

function requestingApiData(endpoint) {
	return {
		type: CONSTANTS.REQUEST_IN_PROGRESS,
		endpoint
	}
}

function apiResponseReceived(response, type) {
	return {
		type,
		response
	}
}

function onSuccess(response) {
	return parseJSON(response)
}

function onError(err) {
	console.log('error', err)
}

function parseJSON(response) {
  return response.json()
}

function onServiceCall(endpoint, type) {

	return dispatch => {
		// tell the application that we are making a service call
		dispatch(requestingApiData(endpoint))

		return fetchInformation(endpoint, onSuccess, onError)
			.then(json => {
				dispatch(apiResponseReceived(json, type))
			})
	}
}

function fetchInformation(endpoint, onSuccess, onError) {

	return fetch(`/api/${endpoint}`, {
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
			}
		})
		.then(onSuccess)
		.catch(onError)
}
