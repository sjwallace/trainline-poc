import { RECEIVED_DEPARTURES } from '../constants/constants'
import trainCodes from '../json/trainCodes'
import { isOnTime } from '../utils/timing'

export function normalize(departure) {

	if (departure && departure.transportMode === 'TRAIN') {

		let { scheduledTime } = departure.scheduledInfo
		let { realTime = null, realTimePlatform = '-' } = departure.realTimeUpdatesInfo && departure.realTimeUpdatesInfo.realTimeServiceInfo || {}

		return {
			realTimePlatform,
			serviceOperator: departure.serviceOperator,
			destination: trainCodes[departure.destinationList[0].crs] || '',
			scheduledArrival: new Date(scheduledTime).toLocaleTimeString('en-GB').substr(0,5),
			onTime: isOnTime(scheduledTime, realTime),
			callingPatternUrl: departure.callingPatternUrl
		}
	}

	return false;
}

export default function departure(state = [], action = {}) {
	switch (action.type) {
		case RECEIVED_DEPARTURES:
			return (action.response && action.response.services || [])
				.map(normalize)
				.filter(destination => {return destination})
		default:
			return state
	}
}
