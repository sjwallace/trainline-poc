import React from 'react'
import App from './components/app'
import DeparturesContainer from './containers/departuresContainer'
import CallingPatternContainer from './containers/callingPatternContainer'
import DepartureStore from './reducers'
import { render } from 'react-dom'
import { Router, Route, browserHistory, IndexRoute } from 'react-router'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import { createStore, applyMiddleware } from 'redux'

let store = createStore(DepartureStore, applyMiddleware(thunk))

render((
	<Provider store={store}>
		<Router history={browserHistory}>
			<Route
				path="/"
				component={App}
			>
				<IndexRoute component={DeparturesContainer} />
				<Route
					path="/departures"
					component={DeparturesContainer}
				/>
				<Route
					path="/callingPattern/:serviceIdentifier/:date"
					component={CallingPatternContainer}
				/>
			</Route>
		</Router>
	</Provider>
), document.getElementById('app'))
