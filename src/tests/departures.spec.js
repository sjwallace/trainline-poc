import departures from '../reducers/departures'
import { __RewireAPI__ as DeparturesRewireAPI } from '../reducers/departures'

let stub = {
	services: [{
		"serviceIdentifier": "W92753",
		"serviceOperator": "SW",
		"transportMode": "TRAIN",
		"scheduledInfo": {
			"scheduledTime": "2016-10-28T23:50:00+01:00"
		},
		"callingType": "PickUp",
		"destinationList": [{
			"crs": "GLD"
		}],
		"via": "via Cobham",
		"realTimeUpdatesInfo": {
			"realTimeServiceInfo": {
				"realTime": "2016-10-28T23:50:00+01:00",
				"realTimePlatform": "4",
				"realTimeFlag": "Estimate"
			}
		},
		"callingPatternUrl": "https://realtime.thetrainline.com/callingPattern/W92753/2016-10-28"
	}]
}

describe('Departures Reducer', () => {

	it('should return state when the action type is not supplied', () => {
		expect(departures()).toEqual(jasmine.any(Array))
		expect(departures().length).toEqual(0)

		expect(departures({})).toEqual(jasmine.any(Object))
		expect(Object.keys(departures({})).length).toEqual(0)

		expect(departures({'foo': 'bar'}, {type: 'baz'})).toEqual({'foo': 'bar'})
	})

	it('should return an empty array when each departure does not have a transport mode of type train', () => {

		let deps = departures([], {response: { services: [{"transportMode": "BUS"}] }, type: 'RECEIVED_DEPARTURES'})

		expect(deps).toEqual(jasmine.any(Array))
		expect(deps.length).toEqual(0)
	})

	it('should return an normalized array when a departure list is supplied', () => {

		let deps = departures([], {response: { services: stub.services }, type: 'RECEIVED_DEPARTURES'})
		let dep = deps[0];

		expect(deps).toEqual(jasmine.any(Array))
		expect(deps.length).toEqual(1)
		expect(Object.keys(deps[0]).length).toEqual(6)
		expect(dep.realTimePlatform).toEqual('4')
		expect(dep.serviceOperator).toEqual('SW')
		expect(dep.destination).toEqual('Guildford')
		expect(dep.scheduledArrival).toEqual('23:50')
		expect(dep.onTime).toEqual(true)
		expect(dep.callingPatternUrl).toEqual('https://realtime.thetrainline.com/callingPattern/W92753/2016-10-28')
	})

	describe('calls the reducing function when the appropriate action type is supplied', () => {

		var spy = jasmine.createSpy('spy')

		beforeEach(() => {
			DeparturesRewireAPI.__Rewire__('normalize', spy)
		})

		afterEach(() => {
			spy.calls.reset()
		})

		it('should not call the reducing function when the action type is not recognized', () => {

			departures({'foo': 'bar'}, {type: 'baz'})
			expect(spy.calls.count()).toEqual(0)
			DeparturesRewireAPI.__ResetDependency__('normalize')
		})

		it('should call the reducing function when the action type is recognized', () => {

			departures([], {response: { services: stub.services }, type: 'RECEIVED_DEPARTURES'})
			expect(spy.calls.count()).toEqual(1)
			DeparturesRewireAPI.__ResetDependency__('normalize')
		})
	})
})
