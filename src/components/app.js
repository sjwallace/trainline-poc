import React from 'react'

const propTypes = {
	children: React.PropTypes.object
}

const defaultProps = {
	children: {}
}

const app = ({children}) => {
	return (
		<div>
			{children}
		</div>
	)
}

app.propTypes = propTypes
app.defaultProps = defaultProps

export default app
