/* eslint-disable */
/* jscs: disable */
// Karma configuration
var path = require('path');

module.exports = function (config) {
	config.set({

		// base path that will be used to resolve all patterns (eg. files, exclude)
		basePath: '../../',

		// frameworks to use
		// available frameworks: https://npmjs.org/browse/keyword/karma-adapter
		frameworks: [
			'jasmine'
		],

		preprocessors: {
			// add webpack as preprocessor
			'src/tests/test_index.js': ['webpack', 'sourcemap']
		},

		webpack: {
			devtool: 'eval',
			module: {
				loaders: [{
					test: /\.jsx?$/,
					exclude: [
						path.resolve('node_modules'),
						path.resolve('build'),
						path.resolve('dist'),
						path.resolve('src/components/tests/index.js')
					],
					loader: 'babel',
					query: {
						presets: ['react', 'es2015', 'stage-2'],
						plugins: ['babel-plugin-rewire']
					}
				},
				{
					test: /\.scss$/,
					loaders: ["style", "css", "sass"]
				},
				{
					test: /\.json$/,
					loaders: ["json"]
				}],
				postLoaders: [{
					test: /\.jsx?$/,
					exclude: [
						path.resolve('src/tests'),
						path.resolve('node_modules')
					],
					loader: 'istanbul-instrumenter'
				}]
			},
			externals: {
				'react/lib/ExecutionEnvironment': true,
				'react/lib/ReactContext': true
			}
		},

		webpackMiddleware: {
			stats: 'none'
		},
		// list of files / patterns to load in the browser
		files: [
			'node_modules/babel-polyfill/dist/polyfill.js',
			'node_modules/phantomjs-polyfill/bind-polyfill.js',
			'node_modules/phantomjs-polyfill-object-assign/object-assign-polyfill.js',
			// only specify one entry point and require all tests in there
			'src/tests/test_index.js'
		],

		// test results reporter to use
		// possible values: 'dots', 'progress'
		// available reporters: https://npmjs.org/browse/keyword/karma-reporter
		reporters: ['tape', 'kjhtml', 'coverage'],

		coverageReporter: {
			dir: 'dist/coverage',
			reporters: [
				{ type: 'json', subdir: '.', file: 'coverage.json' },
				{ type: 'html', subdir: 'html' }
			]
		},

		// web server port
		port: 9876,

		// level of logging
		// possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
		logLevel: config.LOG_DISABLE,

		// enable / disable watching file and executing tests whenever any file changes
		autoWatch: false,

		browserNoActivityTimeout: 60000,

		// start these browsers
		// available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
		browsers: ['PhantomJS'],
		// Continuous Integration mode
		// if true, Karma captures browsers, runs the tests and exits
		singleRun: true

	});
}
