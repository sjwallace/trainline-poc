import { normalize } from '../reducers/departures'

let departureStub = {
	"serviceOperator": "SW",
	"transportMode": "TRAIN",
	"scheduledInfo": {
		"scheduledTime": "2016-10-28T23:50:00+01:00"
	},
	"destinationList": [{
		"crs": "GLD"
	}],
	"realTimeUpdatesInfo": {
		"realTimeServiceInfo": {
			"realTime": "2016-10-28T23:50:00+01:00",
			"realTimePlatform": "4",
			"realTimeFlag": "Estimate"
		}
	},
	"callingPatternUrl": "https://realtime.thetrainline.com/callingPattern/W92753/2016-10-28"
}

describe('normalize', () => {

	it('should return false when no departure information is given or the transportMode is not TRAIN', () => {
		expect(normalize()).toEqual(false)
		expect(normalize({})).toEqual(false)
		expect(normalize({transportMode: 'BUS'})).toEqual(false)
	})

	it('should return a normalized object containing departure information', () => {
		let departure = normalize(departureStub)

		expect(departure).toEqual(jasmine.any(Object))
		expect(Object.keys(departure).length).toEqual(6)
		expect(departure.realTimePlatform).toEqual("4")
		expect(departure.serviceOperator).toEqual('SW')
		expect(departure.destination).toEqual('Guildford')
		expect(departure.scheduledArrival).toEqual('23:50')
		expect(departure.onTime).toEqual(true)
		expect(departure.callingPatternUrl).toEqual("https://realtime.thetrainline.com/callingPattern/W92753/2016-10-28")

		departureStub.scheduledInfo.scheduledTime = "2016-10-28T23:45:00+01:00"
		departure = normalize(departureStub)
		expect(departure.onTime).toEqual(false)

	})

	it('should return a an empty string when a crs code is not found', () => {

		departureStub.destinationList = [{
			"crs": "foo"
		}]

		expect(normalize(departureStub).destination).toEqual('')
	})
})
