import { connect } from 'react-redux'
import { getCallingPattern } from '../actions/actions'
import Stops from '../components/stops'

const mapStateToProps = state => {
	return { callingPattern: state.callingPattern }
}

const mapDispatchToProps = dispatch => {
	return {
		fetchCallingPattern(path) {
			dispatch(getCallingPattern(path))
		}
	}
}

const CallingPatternContainer = connect(
	mapStateToProps,
	mapDispatchToProps
)(Stops)

export default CallingPatternContainer
