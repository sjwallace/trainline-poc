import departure from './departures'
import callingPattern from './callingPattern'

function DepartureStore(state = {}, action) {

	return {
		departures: departure(state.departures, action),
		callingPattern: callingPattern(state.callingPattern, action)
	}
}

export default DepartureStore
