import { connect } from 'react-redux'
import { getDepartures } from '../actions/actions'
import Departures from '../components/departures'

const mapStateToProps = state => {
	return { departures: state.departures }
}

const mapDispatchToProps = dispatch => {
	return {
		fetchDepartures(payload) {
			dispatch(getDepartures(payload))
		}
	}
}

const DeparturesContainer = connect(
	mapStateToProps,
	mapDispatchToProps
)(Departures)

export default DeparturesContainer
