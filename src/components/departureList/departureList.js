import React from 'react'
import CallingLink from '../callingLink/callingLink'
require("!style!css!sass!./_departures.scss")
require("!style!css!semantic-ui-grid/grid.css")

const propTypes = {
	departures: React.PropTypes.array,
	filterBy: React.PropTypes.func
}

const defaultProps = {
	departures: [],
	filterBy: () => { return true }
}

const DepartureList = ({ departures = [], filterBy }) => {

	return !departures.length ? null : (
		<ul className="departuresList column wide">
			{
				departures.map((departure, index) => {
					let { callingPatternUrl, scheduledArrival, destination,
							serviceOperator, realTimePlatform, onTime
						} = departure

					if (filterBy && typeof filterBy === 'function' && filterBy(departure)) {

						return (
							<CallingLink
								path={callingPatternUrl}
								key={index}
							>
								<li
									className="ui padded centered grid"
									tabIndex="0"
								>
									<div className="two wide column arrivalTime left aligned">
										{scheduledArrival}
									</div>
									<div className="six wide column destination">
										<div className="row">
											<span className="column">
												{destination}
											</span>
										</div>
										<div className="row">
											<span className="column">
												{serviceOperator}
											</span>
										</div>
									</div>
									<div className="three wide column right aligned platform">
										<div className="row">
											<span className="column platform-num">
												{`Plat. ${realTimePlatform}`}
											</span>
										</div>
										<div className="row">
											<span className="column onSchedule">
												{onTime ? 'onTime' : ''}
											</span>
										</div>
									</div>
								</li>
							</CallingLink>
						)
					}
					return null
				})
			}
		</ul>
	)
}

DepartureList.propTypes = propTypes
DepartureList.defaultProps = defaultProps

export default DepartureList
